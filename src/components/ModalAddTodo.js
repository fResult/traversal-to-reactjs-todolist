import { Modal, Form, Input, Button, Checkbox } from "antd";
import { useForm } from "antd/lib/form/Form";
import React from "react";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

function ModalAddTodo({ onAddTodo, isVisible, onCancel }) {
  const [form] = useForm()
  return (
    <Modal
      title="Add New Todo"
      visible={isVisible}
      onOk={() => onAddTodo(form.getFieldsValue())}
      onCancel={onCancel}
    >
      <Form
        form={form}
        {...layout}
        name="addNewTodo"
        initialValues={{ remember: true }}
        onFinish={onAddTodo}
      >
        <Form.Item
          label="Task"
          name="text"
          rules={[{ required: true, message: "Please input Task!" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Due Date"
          name="dueDate"
          rules={[{ required: true, message: "Please input Due Date!" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Add 
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default ModalAddTodo;
