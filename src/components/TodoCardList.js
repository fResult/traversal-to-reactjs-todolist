import React from "react";
import { List, Typography } from "antd";
import TodoCard from "./TodoCard";

const { Title } = Typography;

function TodoCardList({ todoItems, header }) {
  return (
    <List header={<Title level={3}>{header}</Title>}>
      {todoItems.map((todo) => {
        return (
          <List.Item key={todo.id}>
            <TodoCard todo={todo} />
          </List.Item>
        );
      })}
    </List>
  );
}

export default TodoCardList;
