# 🗺 Traversal to ReactJS (TodoList workshop) 🚶‍♂️

## ⚙ Feature ที่ (คาดว่าจะ) มีใน Project
- [x] แสดงผลรายการ Todo list แบ่งเป็น 3 ส่วน
   1. [x] `TODO` (ทุก task ที่สร้างใหม่จะมาแสดงที่นี่ก่อน)
   2. [x] `DOING` (แสดงรายการ task ที่กำลังทำ)
   3. [x] `DONE` (แสดงรายการ task ที่ทำเสร็จแล้ว)
- [x] เพิ่ม Todo
- [ ] แก้ไข Todo
- [ ] ลบ Todo
- [ ] ค้นหา Todo list และแสดงผลการค้นหา ด้วยชื่อ task
- [ ] ย้าย Task ระหว่าง 3 ส่วนของ (TODO-DOING-DONE) ด้วยวิธี Drag and Drop

### 📈 ผลลัพธ์ปัจจุบัน ตามที่ Project นี้ได้ทำไปแล้ว
https://traversal-to-reactjs-todolist.vercel.app

## ⚒ เครื่องมือที่ (คาดว่าจะ) ใช้ใน Project
- [x] `ReactJS`
- [x] `create-react-app` 
- [x] `Ant Design` (UI Library)
- [ ] `Styled Component` (CSS-in-JS Library)
- [ ] `React Router`
- [ ] Some Drag and Drop library

## 🖼 ภาพตัวอย่างหน้าจอ Todo list ที่พี่เอิร์ธให้มา
![xyz](designed/prototype.png)

### 📝 Past activities
**4 มีนาคม 2564**
- การ Setup Project ด้วย `create-react-app`
- การใช้งาน `npm`/`yarn` เบื้องต้น
- การใช้งาน `Ant Design` พร้อมวิธีดู doc เบื้องต้น
  - การ setup Antd
  - Button
  - Card
  - Typography
  - Grid System (Row/Col)
  - List
  - Modal
  - Form
- การเขียน CSS แบบ `CSS-in-JS` เบื้องต้น
- การสื่อสารข้อมูลของ state ระหว่าง parent component และ child component
- การใช้ `useState` และการทำงานของ state
- การเพิ่มข้อมูล Todo และนำ Task ใหม่มาแสดงบนหน้าจอ

### ⏭ Next activities
**สิ่งที่คาดว่าจะมีสอนต่อ**
- การแก้ไขข้อมูล Todo และแสดงผลบนหน้าจอ
- การลบข้อมูล Todo และแสดงผลบนหน้าจอ
- การค้นหา Todo และแสดงผลการค้นหาบนหน้าจอ
- การสื่อสารข้อมูลของ state ระหว่าง child component ด้วยกัน
- การทำ drag & drop โดยใช้งาน library
- การใช้ `React Router`
  - การเปลี่ยน page
  - การส่งข้อมูลระหว่าง page
